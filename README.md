# Advent Of Code 2022

## Running tests

The tests are implemented using JUnit.
The tests are parameterized and the input filename and expected output for each test case can be found in th TestCases
class.
The test in the DayTest class will run all implemented tests.
To run tests for individual days run the test class in the corresponding folder.

## Running program

Running aoc.App will run both puzzles of each day.
To run only a specific day, enter a nu

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
