package aoc;

import java.util.List;
public abstract class Day {

    public Day() {}

    public abstract String puzzle1(List<String> input);

    public abstract String puzzle2(List<String> input);

    public abstract String getName();
}
