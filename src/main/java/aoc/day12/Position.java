package aoc.day12;

public class Position implements Comparable<Position> {

    private final int x;
    private final int y;
    private final char height;
    private char visited = '.';
    private Position next;
    private int minimumRemainingDistance;
    private int distanceFromGoal = Integer.MAX_VALUE;


    public Position(int x, int y, char height) {
        this.x = x;
        this.y = y;
        this.height = height;
    }

    @Override
    public int compareTo(Position otherPos) {
        return Integer.compare(getTotalCostEstimate(), otherPos.getTotalCostEstimate());
    }

    public void calculateMinimumRemainingDistance(Position start) {
        // Heuristic for A*
        this.minimumRemainingDistance = this.height - start.getHeight();
    }

    public boolean isVisited() {
        return this.visited != '.';
    }

    public void setVisited() {
        if (this.next != null) {
            if (next.getYCoord() > this.y) {
                this.visited = 'v';
            } else if (next.getYCoord() < this.y) {
                this.visited = '^';
            } else if (next.getXCoord() > this.x) {
                this.visited = '>';
            } else if (next.getXCoord() < this.x) {
                this.visited = '<';
            }
            next.setVisited();
        }
    }

    public void setNext(Position next) {
        this.next = next;
        this.setVisited();
    }

    public void setDistanceFromGoal(int distance) {
        this.distanceFromGoal = distance;
    }

    public int getDistanceFromGoal() {
        return this.distanceFromGoal;
    }

    private int getTotalCostEstimate() {
        return this.distanceFromGoal + this.minimumRemainingDistance;
    }

    public char getHeight() {
        return this.height;
    }

    public int getXCoord() {
        return this.x;
    }

    public int getYCoord() {
        return this.y;
    }

    public char getVisited() {
        return this.visited;
    }

    public Position getNext() {
        return this.next;
    }

}
