package aoc.day12;

import aoc.Day;

import java.util.List;

public class HillClimbingAlgorithm extends Day {
    @Override
    public String puzzle1(List<String> input) {
        HeightMap heightMap = new HeightMap(input, false);
        System.out.println(heightMap);
        String shortestPathToGoal = Integer.toString(heightMap.findShortestPathToGoal());
        System.out.println(heightMap);

        return shortestPathToGoal;
    }

    @Override
    public String puzzle2(List<String> input) {
        HeightMap heightMap = new HeightMap(input, true);
        System.out.println(heightMap);
        String shortestPathToGoal = Integer.toString(heightMap.findShortestPathToGoal());
        System.out.println(heightMap);
        return shortestPathToGoal;
    }

    @Override
    public String getName() {
        return "HillClimbingAlgorithm";
    }
}
