package aoc.day12;

import aoc.utils.ConsoleColors;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class HeightMap {
    private final List<List<Position>> map;
    private final List<Position> startingPositions;
    private Position goalPos;
    private final List<Position> pathToGoal = new ArrayList<>();

    public HeightMap(List<String> positions, boolean SIsStart) {
        this.startingPositions = new LinkedList<>();
        this.map = new ArrayList<>(positions.size());
        this.buildMap(positions, SIsStart);
        this.calculateMinimumDistancesToGoal();
    }

    private void buildMap(List<String> positions, boolean SIsStart) {
        final char startingHeight = 'a';
        final char goalHeight = 'z' + 1;

        for (int y = 0; y < positions.size(); y++) {
            String row = positions.get(y);
            List<Position> mapRow = new ArrayList<>(row.length());

            for (int x = 0; x < row.length(); x++) {
                char height = row.charAt(x);
                Position position;
                if (height == 'S' || SIsStart && height == 'a') {
                    height = startingHeight;
                    position = new Position(x, y, height);
                    this.startingPositions.add(position);
                } else if (height == 'E') {
                    height = goalHeight;
                    position = new Position(x, y, height);
                    this.goalPos = position;
                    this.goalPos.setDistanceFromGoal(0);
                } else {
                    position = new Position(x, y, height);
                }
                mapRow.add(position);
            }
            this.map.add(mapRow);
        }
    }

    private void calculateMinimumDistancesToGoal() {
        for (List<Position> row : this.map) {
            for (Position position : row) {
                position.calculateMinimumRemainingDistance(this.startingPositions.get(0));
            }
        }
    }

    private void setPathToGoal(Position from) {
        Position currentPos = from;
        do {
            this.pathToGoal.add(currentPos);
            currentPos = currentPos.getNext();
        } while (currentPos != null);
    }

    public int findShortestPathToGoal() {
        // Searches from the goal using A* to the first encountered (closest) starting position.
        PriorityQueue<Position> discoveredPositions = new PriorityQueue<>();
        Position currentPos = this.goalPos;
        while (!this.startingPositions.contains(currentPos)) {
            for (Position neighbour : findValidNeighbours(currentPos)) {
                if (neighbour.getDistanceFromGoal() > currentPos.getDistanceFromGoal() + 1) {
                    discoveredPositions.remove(neighbour);
                    neighbour.setDistanceFromGoal(currentPos.getDistanceFromGoal() + 1);
                    neighbour.setNext(currentPos);
                    discoveredPositions.add(neighbour);
                }
            }
            currentPos = discoveredPositions.remove();
        }
        setPathToGoal(currentPos);
        return currentPos.getDistanceFromGoal();
    }

    private List<Position> findValidNeighbours(Position pos) {
        List<Position> neighbours = new ArrayList<>();
        if (pos.getYCoord() > 0) {
            neighbours.add(this.map.get(pos.getYCoord() - 1).get(pos.getXCoord()));
        }
        if (pos.getYCoord() < map.size() - 1) {
            neighbours.add(this.map.get(pos.getYCoord() + 1).get(pos.getXCoord()));
        }
        if (pos.getXCoord() > 0) {
            neighbours.add(this.map.get(pos.getYCoord()).get(pos.getXCoord() - 1));
        }
        if (pos.getXCoord() < this.map.get(pos.getYCoord()).size() - 1) {
            neighbours.add(this.map.get(pos.getYCoord()).get(pos.getXCoord() + 1));
        }
        //If a position has already been visited, the shortest path is already found
        //If the height of the next node is more than 1 below the current (seen from the goal) it cannot be reached.
        neighbours.removeIf(neighbour -> neighbour.isVisited() || pos.getHeight() - neighbour.getHeight() > 1);
        return neighbours;
    }


    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (List<Position> row : this.map) {
            for (Position pos : row) {
                if (pos == goalPos) {
                    out.append(ConsoleColors.GREEN).append('X').append(ConsoleColors.RESET);
                    continue;
                }
                if (this.startingPositions.contains(pos) && pos.isVisited()) {
                    out.append(ConsoleColors.GREEN).append('*').append(ConsoleColors.RESET);
                    continue;
                }

                if (this.pathToGoal.contains(pos)) {
                    out.append(ConsoleColors.GREEN);
                    out.append(pos.getVisited());
                } else if (pos.isVisited()) {
                    out.append(ConsoleColors.YELLOW);
                    out.append(pos.getHeight());
                } else {
                    out.append(ConsoleColors.RED);
                    out.append(pos.getHeight());
                }
                out.append(ConsoleColors.RESET);
            }
            out.append("\n");
        }
        return out.toString();
    }
}
