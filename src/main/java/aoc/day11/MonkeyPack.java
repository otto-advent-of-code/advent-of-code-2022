package aoc.day11;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MonkeyPack {

    private final List<Monkey> monkeys = new ArrayList<>();

    public void passItemTo(Item item, int to) {
        this.monkeys.get(to).receiveItem(item);
    }

    public void addMonkey(Monkey monkey) {
        this.monkeys.add(monkey);
    }

    public void performRound() {
        for (Monkey monkey : this.monkeys) {
            monkey.takeTurn(this);
        }
    }

    public long calculateMonkeyBusiness() {
        List<Monkey> sortedMonkeys = (new ArrayList<>(this.monkeys));
        sortedMonkeys.sort(Comparator.comparing(Monkey::getItemsInspected));
        System.out.println(sortedMonkeys.get(sortedMonkeys.size() - 1).getItemsInspected());
        System.out.println(sortedMonkeys.get(sortedMonkeys.size() - 2).getItemsInspected());
        return (long) (sortedMonkeys.get(sortedMonkeys.size() - 1).getItemsInspected()) *
                sortedMonkeys.get(sortedMonkeys.size() - 2).getItemsInspected();

    }
}
