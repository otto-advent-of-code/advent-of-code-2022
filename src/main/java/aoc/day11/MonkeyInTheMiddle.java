package aoc.day11;

import aoc.Day;

import java.util.*;

public class MonkeyInTheMiddle extends Day {

    private Monkey parseMonkey(List<String> monkeyString, boolean managed) {
        Queue<Item> startingItems = parseStartingItems(monkeyString.get(1), managed);
        Item.Operation operation = parseOperator(monkeyString.get(2));
        int operator = parseOperand(monkeyString.get(2));
        Test test = parseTest(monkeyString.subList(3, monkeyString.size()));

        return new Monkey(startingItems, operation, operator, test);
    }

    private Queue<Item> parseStartingItems(String itemString, boolean managed) {
        Queue<Item> startingItems = new ArrayDeque<>();

        itemString = itemString.split(":")[1];
        for (String s : itemString.split(",")) {
            int itemValue = Integer.parseInt(s.strip());
            if (managed) {
                ManagedItem item = new ManagedItem(itemValue);
                startingItems.add(item);
            } else {
                UnmanagedItem item = new UnmanagedItem(itemValue);
                startingItems.add(item);
            }

        }
        return startingItems;
    }

    private Item.Operation parseOperator(String operatonString) {
        Item.Operation operation;
        List<String> operationSplit = Arrays.stream(operatonString.split("= ")[1].split(" ")).toList();
        String operand = operationSplit.get(2);
        String opSign = operationSplit.get(1);

        if (operand.equals("old")) { // Square operation
            operation = Item.Operation.SQUARE;
            return operation;
        }

        if (opSign.equals("*")) {
            operation = Item.Operation.MULTIPLY;
            return operation;
        }
        if (opSign.equals("+")) {
            operation = Item.Operation.ADD;
            return operation;
        }
        return null;
    }

    private int parseOperand(String operatorString) {
        List<String> operationSplit = Arrays.stream(operatorString.split("= ")[1].split(" ")).toList();
        String operand = operationSplit.get(2);

        if (operand.equals("old")) {
            return 0;
        }

        return Integer.parseInt(operand);
    }

    private Test parseTest(List<String> testStrings) {
        int condition = Integer.parseInt(testStrings.get(0).strip().split(" ")[3]);
        int ifTrue = Integer.parseInt(testStrings.get(1).strip().split(" ")[5]);
        int ifFalse = Integer.parseInt(testStrings.get(2).strip().split(" ")[5]);

        return new Test(condition, ifTrue, ifFalse);
    }


    @Override
    public String puzzle1(List<String> input) {
        MonkeyPack monkeys = new MonkeyPack();
        for (int i = 0; i < input.size(); i += 7) {
            List<String> monkeyStrings = input.subList(i, i + 6);
            monkeys.addMonkey(parseMonkey(monkeyStrings, true));
        }

        for (int i = 0; i < 20; i++) {
            monkeys.performRound();
        }
        return Long.toString(monkeys.calculateMonkeyBusiness());
    }

    @Override
    public String puzzle2(List<String> input) {
        MonkeyPack monkeys = new MonkeyPack();

        for (int i = 3; i < input.size(); i += 7) {
            int divisor = Integer.parseInt(input.get(i).split(" ")[5]);
            UnmanagedItem.addDivisor(divisor);
        }

        for (int i = 0; i < input.size(); i += 7) {
            List<String> monkeyStrings = input.subList(i, i + 6);
            monkeys.addMonkey(parseMonkey(monkeyStrings, false));
        }

        for (int i = 0; i < 10000; i++) {
            monkeys.performRound();
        }
        return Long.toString(monkeys.calculateMonkeyBusiness());
    }

    @Override
    public String getName() {
        return "MonkeyInTheMiddle";
    }
}
