package aoc.day11;

public class Test {

    private final int condition;
    private final int ifTrue;
    private final int ifFalse;

    public Test(int condition, int ifTrue, int ifFalse) {
        this.condition = condition;
        this.ifTrue = ifTrue;
        this.ifFalse = ifFalse;
    }

    public int testItem(Item item) {
        if (item.isDivisibleBy(condition)) {
            return ifTrue;
        }

        return ifFalse;
    }
}
