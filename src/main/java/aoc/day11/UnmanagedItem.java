package aoc.day11;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UnmanagedItem extends Item {

    static List<Integer> divisors = new ArrayList<>();
    Map<Integer, Integer> remainders;


    static void addDivisor(int divisor) {
        divisors.add(divisor);
    }

    public UnmanagedItem(int value) {
        this.remainders = new HashMap<>();

        for (int divisor : divisors) {
            remainders.put(divisor, value % divisor);
        }
    }

    public boolean isDivisibleBy(int divisor) {
        return remainders.containsKey(divisor) && remainders.get(divisor) == 0;
    }

    public void add(int value) {
        for (Integer divisor : remainders.keySet()) {
            int newRemainder = (remainders.get(divisor) + value) % divisor;
            remainders.put(divisor, newRemainder);
        }
    }

    public void multiply(int value) {
        for (Integer divisor : remainders.keySet()) {
            int newRemainder = (remainders.get(divisor) * value) % divisor;
            remainders.put(divisor, newRemainder);
        }
    }

    public void square() {
        for (Integer divisor : remainders.keySet()) {
            int newRemainder = (remainders.get(divisor) * remainders.get(divisor)) % divisor;
            remainders.put(divisor, newRemainder);
        }
    }
}
