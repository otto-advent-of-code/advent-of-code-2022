package aoc.day11;

public abstract class Item {

    enum Operation {
        ADD,
        MULTIPLY,
        SQUARE
    }

    public void execute(Operation operation, int operand) {
        switch (operation) {
            case ADD -> this.add(operand);
            case MULTIPLY -> this.multiply(operand);
            case SQUARE -> this.square();
        }
    }

    public abstract boolean isDivisibleBy(int divisor);

    public abstract void add(int operand);

    public abstract void multiply(int operand);

    public abstract void square();

}
