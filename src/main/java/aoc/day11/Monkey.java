package aoc.day11;

import java.util.Queue;

public class Monkey {


    private final Queue<Item> items;
    private final Item.Operation operation;
    private final int operand;
    private final Test test;
    private int itemsInspected;

    Monkey(Queue<Item> startingItems, Item.Operation operation, int operand, Test test) {
        this.items = startingItems;
        this.operation = operation;
        this.operand = operand;
        this.test = test;
        this.itemsInspected = 0;
    }

    public void takeTurn(MonkeyPack pack) {
        while (!this.items.isEmpty()) {
            Item item = this.items.remove();
            item.execute(operation, operand);
            throwItem(item, pack);
            this.itemsInspected++;
        }
    }

    private void throwItem(Item item, MonkeyPack pack) {
        pack.passItemTo(item, this.test.testItem(item));
    }

    public void receiveItem(Item item) {
        this.items.add(item);
    }

    public int getItemsInspected() {
        return itemsInspected;
    }
}
