package aoc.day11;

public class ManagedItem extends Item {

    private int value;

    public ManagedItem(int value) {
        this.value = value;
    }


    public boolean isDivisibleBy(int divisor) {
        return value % divisor == 0;
    }

    public void add(int value) {
        this.value += value;
        this.divide(3);
    }

    public void multiply(int value) {
        this.value *= value;
        this.divide(3);
    }

    public void square() {
        this.value *= this.value;
        this.divide(3);
    }

    public void divide(int operand) {
        this.value /= operand;
    }
}
