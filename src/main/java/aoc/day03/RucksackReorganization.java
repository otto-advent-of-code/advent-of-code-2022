package aoc.day03;

import aoc.Day;
import aoc.utils.util;

import java.util.ArrayList;
import java.util.List;

public class RucksackReorganization extends Day {

    static private int priorityOf(char item) {
        int priority;
        if (Character.isUpperCase(item)) {
            priority = Character.getNumericValue(item) - Character.getNumericValue('A') + 27;
        } else {
            priority = Character.getNumericValue(item) - Character.getNumericValue('a') + 1;
        }
            return priority;
    }

    static private List<List<String>> groupRucksacks(List<String> rucksacks) {
        List<List<String>> groups = new ArrayList<>();
        while (!rucksacks.isEmpty()) {
            groups.add(new ArrayList<>(rucksacks.subList(0,3)));
            rucksacks.remove(0);
            rucksacks.remove(0);
            rucksacks.remove(0);
        }
        return groups;
    }

    @Override
    public String puzzle1(List<String> input) {
        int prioritySum = 0;
        for(String rucksack : input) {
            String firstCompartment = rucksack.substring(0, rucksack.length()/2);
            String secondCompartment = rucksack.substring(rucksack.length()/2);

            char sharedItem = util.stringIntersection(firstCompartment, secondCompartment).charAt(0);
            prioritySum += priorityOf(sharedItem);
        }
        return Integer.toString(prioritySum);
    }

    @Override
    public String puzzle2(List<String> input) {
        int prioritySum = 0;
        for(List<String> group : groupRucksacks(input)) {
            char commonItem = util.stringIntersection(util.stringIntersection(group.get(0), group.get(1)), group.get(2)).charAt(0);
            prioritySum += priorityOf(commonItem);
        }
        return Integer.toString(prioritySum);
    }

    @Override
    public String getName() {
        return "RucksackReorganization";
    }
}
