package aoc.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static java.lang.Math.sqrt;

public class util {


    public static String stringIntersection(String a, String b) {
        HashSet<Character> h1 = new HashSet<>(), h2 = new HashSet<>();
        for (Character c : a.toCharArray()) {
            h1.add(c);
        }
        for (Character c : b.toCharArray()) {
            h2.add(c);
        }
        h1.retainAll(h2);

        StringBuilder sb = new StringBuilder();
        for (Character c : h1) {
            sb.append(c);
        }
        return sb.toString();
    }

    public static List<Integer> getPrimeFactors(int n) {
        List<Integer> primeFactors = new ArrayList<>();

        if (n <= 0) {
            return primeFactors;
        }

        if (n % 2 == 0) {
            primeFactors.add(2);
            n /= 2;
        }

        for (int i = 3; i < sqrt(n); i += 2) {
            while (n % i == 0) {
                primeFactors.add(i);
                n /= i;
            }
        }

        if (n > 2) {
            primeFactors.add(n);
        }

        return primeFactors;
    }
}
