package aoc.utils;

public class Range {

    private final int lowerBound;
    private final int upperBound;

    public Range(int lowerBound, int upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    public boolean contains(int number) {
        return (number >= lowerBound && number <= upperBound);
    }

    public boolean contains(Range range) {
        return (range.lowerBound >= this.lowerBound && range.upperBound <= this.upperBound);
    }

    public boolean overlaps(Range range) {
        return this.contains(range.lowerBound) ||
                this.contains(range.upperBound) ||
                range.contains(this.lowerBound) ||
                range.contains(this.upperBound);
    }
}
