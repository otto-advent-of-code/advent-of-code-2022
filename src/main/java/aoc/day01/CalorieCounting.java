package aoc.day01;

import aoc.Day;
import java.util.*;

public class CalorieCounting extends Day {

    @Override
    public String puzzle1(List<String> input) {
        input.add("");
        int loadedElf = 0;
        int elfFood = 0;
        for (String entry: input) {
            if (!entry.equals("")) {
                elfFood += Integer.parseInt(entry);
                continue;
            }
            if (elfFood > loadedElf) {
                loadedElf = elfFood;
            }
            elfFood = 0;
        }
        return Integer.toString(loadedElf);
    }

    @Override
    public String puzzle2(List<String> input) {
        input.add("");
        List<Integer> elves = new ArrayList<>();
        int elf = 0;

        for (String entry: input) {
            if (entry.equals("")) {
                elves.add(elf);
                elf = 0;
                continue;
            }

            elf += Integer.parseInt(entry);
        }

        Collections.sort(elves);
        Collections.reverse(elves);
        return Integer.toString(elves.get(0) + elves.get(1) + elves.get(2));
    }

    @Override
    public String getName(){
        return "CalorieCounting";
    }

}
