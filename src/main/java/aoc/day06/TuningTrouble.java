package aoc.day06;

import aoc.Day;

import java.util.ArrayList;
import java.util.List;

public class TuningTrouble extends Day {

    private char[] dataStream;

    private int findFirstUniqueSubstringPos(int size) {
        List<Character> lastThree = new ArrayList<>();
        int marker = 0;
        for(char data : this.dataStream){
            if(lastThree.contains(data)) {
                lastThree = lastThree.subList(lastThree.indexOf(data)+1, lastThree.size());
            }
            lastThree.add(data);
            marker++;
            if (lastThree.size() == size) {
                break;
            }
        }
        return marker;
    }
    @Override
    public String puzzle1(List<String> input) {
        this.dataStream = input.get(0).toCharArray();
        String packetMarker = Integer.toString(findFirstUniqueSubstringPos(4));
        return packetMarker;
    }

    @Override
    public String puzzle2(List<String> input) {
        this.dataStream = input.get(0).toCharArray();
        String packetMarker = Integer.toString(findFirstUniqueSubstringPos(14));
        return packetMarker;
    }

    @Override
    public String getName() {
        return "TuningTrouble";
    }
}
