package aoc;

import aoc.day01.CalorieCounting;
import aoc.day02.RockPaperScissors;
import aoc.day03.RucksackReorganization;
import aoc.day04.CampCleanup;
import aoc.day05.SupplyStacks;
import aoc.day06.TuningTrouble;
import aoc.day07.NoSpaceLeftOnDevice;
import aoc.day08.TreetopTreeHouse;
import aoc.day09.RopeBridge;
import aoc.day10.CathodeRayTube;
import aoc.day11.MonkeyInTheMiddle;
import aoc.day12.HillClimbingAlgorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

import static java.util.stream.Collectors.toList;

public class App {
    public static final Map<Integer, Class<? extends Day>> DAYS;

    static {
        DAYS = new HashMap<>();
        DAYS.put(1, CalorieCounting.class);
        DAYS.put(2, RockPaperScissors.class);
        DAYS.put(3, RucksackReorganization.class);
        DAYS.put(4, CampCleanup.class);
        DAYS.put(5, SupplyStacks.class);
        DAYS.put(6, TuningTrouble.class);
        DAYS.put(7, NoSpaceLeftOnDevice.class);
        DAYS.put(8, TreetopTreeHouse.class);
        DAYS.put(9, RopeBridge.class);
        DAYS.put(10, CathodeRayTube.class);
        DAYS.put(11, MonkeyInTheMiddle.class);
        DAYS.put(12, HillClimbingAlgorithm.class);
    }

    private static List<String> loadInput(Day day) {
        String fileName = day.getName() + ".in";

        try (BufferedReader fileReader = new BufferedReader((new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(fileName)))))) {
            return fileReader.lines().collect(toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static void main(String[] args)
            throws NoSuchMethodException,
            InvocationTargetException,
            InstantiationException,
            IllegalAccessException {

        int dayToRun = 0;
        int puzzleToRun = 0;

        if (args.length != 0) {
            dayToRun = Integer.parseInt(args[0]);

            if (args.length > 1) {
                puzzleToRun = Integer.parseInt((args[1]));
            }
        }

        if (dayToRun == 0) { //Run all implemented days
            for (Map.Entry<Integer, Class<? extends Day>> day : DAYS.entrySet()) {
                if (puzzleToRun == 0 || puzzleToRun == 1) {
                    Day puzzleObj = day.getValue().getDeclaredConstructor().newInstance();
                    String res1 = puzzleObj.puzzle1(loadInput(puzzleObj));
                    System.out.printf("Result for puzzle1 of day %d (%s): %s\n", day.getKey(), day.getValue().getName(), res1);
                }
                if (puzzleToRun == 0 || puzzleToRun == 2) {
                    Day puzzleObj = day.getValue().getDeclaredConstructor().newInstance();
                    String res2 = puzzleObj.puzzle2(loadInput(puzzleObj));
                    System.out.printf("Result for puzzle2 of day %d (%s): %s\n", day.getKey(), day.getValue().getName(), res2);
                }
            }
        } else { //Run specified day
            if (puzzleToRun == 0 || puzzleToRun == 1) {
                Day puzzleObj = DAYS.get(dayToRun).getDeclaredConstructor().newInstance();
                String res1 = puzzleObj.puzzle1(loadInput(puzzleObj));
                System.out.printf("Result for puzzle1 of day %d (%s): %s\n", dayToRun, puzzleObj.getName(), res1);
            }
            if (puzzleToRun == 0 || puzzleToRun == 2) {
                Day puzzleObj = DAYS.get(dayToRun).getDeclaredConstructor().newInstance();
                String res2 = puzzleObj.puzzle2(loadInput(puzzleObj));
                System.out.printf("Result for puzzle2 of day %d (%s): %s\n", dayToRun, puzzleObj.getName(), res2);
            }
        }
    }
}