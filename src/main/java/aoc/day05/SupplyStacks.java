package aoc.day05;

import aoc.Day;

import java.util.*;

public class SupplyStacks extends Day {
    private final List<Stack<Character>> supply;

    public SupplyStacks() {
        this.supply = new ArrayList<>();
    }
    private void populateSupply(List<String> supplyInput) {
        String indexLine = supplyInput.get(supplyInput.size()-1);
        int stackCount = Character.getNumericValue(indexLine.charAt(indexLine.length()-1));

        this.supply.add(new Stack<>()); //0th stack only for padding

        for (int i = 0; i < stackCount; i++) {
            this.supply.add(new Stack<>());
        }

        Collections.reverse(supplyInput);

        for (String line : supplyInput.subList(1, supplyInput.size())) {
            for (int i = 1; i < line.length(); i += 4) {
                char c = line.charAt(i);
                if (c != ' ') {
                    this.supply.get(i/4+1).add(c);
                }
            }
        }
    }

    private void moveCrate(String move) {
        String[] seperated = move.split(" ");
        int numberToMove = Integer.parseInt(seperated[1]);
        int from = Integer.parseInt(seperated[3]);
        int to = Integer.parseInt(seperated[5]);

        for (int i = 0; i < numberToMove; i++) {
            char movedCrate = this.supply.get(from).pop();
            this.supply.get(to).push(movedCrate);
        }
    }

    private void moveMultipleCrates(String move) {
        String[] seperated = move.split(" ");
        int numberToMove = Integer.parseInt(seperated[1]);
        Stack<Character> from = this.supply.get(Integer.parseInt(seperated[3]));
        Stack<Character> to = this.supply.get(Integer.parseInt(seperated[5]));

        List<Character> movedCrates = from.subList(from.size()-numberToMove, from.size());
        to.addAll(movedCrates);

        for (int i=0; i < numberToMove; i++) {
            from.pop();
        }
    }

    private void printStack() {
        int tallestStackSize = 0;
        for (Stack<Character> stack: this.supply) {
            if (stack.size() > tallestStackSize) {
                tallestStackSize = stack.size();
            }
        }
        List<String> lines = new LinkedList<>();
        for (int i=0; i < tallestStackSize; i++) {
            StringBuilder line = new StringBuilder();
            for (Stack<Character> stack: this.supply.subList(1, this.supply.size())) {
                char crate = ' ';
                if(i < stack.size()) {
                    crate = stack.get(i);
                }

                line.append("[" + crate + "] ");
            }
            lines.add(0, line.toString());
        }

        for (String line : lines) {
            System.out.println(line);
        }

        for (int i=1; i < this.supply.size(); i++) {
            System.out.print(" " + i + "  ");
        }
        System.out.println("\n");
    }

    @Override
    public String puzzle1(List<String> input) {
        List<String> initialStacking = input.subList(0, input.indexOf(""));
        List<String> craneMoves = input.subList(input.indexOf("") + 1, input.size());
        populateSupply(initialStacking);

        for (String move: craneMoves) {
            moveCrate(move);
            //printStack();
        }
        StringBuilder ans = new StringBuilder();

        for (Stack<Character> stack: this.supply) {
            if (!stack.isEmpty()) {
                ans.append(stack.pop());
            }
        }
        return ans.toString();
    }

    @Override
    public String puzzle2(List<String> input) {
        List<String> initialStacking = input.subList(0, input.indexOf(""));
        List<String> craneMoves = input.subList(input.indexOf("") + 1, input.size());
        populateSupply(initialStacking);

        for (String move: craneMoves) {
            moveMultipleCrates(move);
            //printStack();
        }
        StringBuilder ans = new StringBuilder();

        for (Stack<Character> stack: this.supply) {
            if (!stack.isEmpty()) {
                ans.append(stack.pop());
            }
        }
        return ans.toString();
    }

    @Override
    public String getName() {
        return "SupplyStacks";
    }
}
