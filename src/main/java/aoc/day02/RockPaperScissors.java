package aoc.day02;

import aoc.Day;

import java.util.List;

public class RockPaperScissors extends Day {

    private enum Shape {
        ROCK,
        PAPER,
        SCISSORS
    }

    private enum Outcome {
        WIN,
        LOSS,
        DRAW
    }

    static private Shape getShape(char c) {
        return switch (c) {
            case 'A', 'X' -> Shape.ROCK;
            case 'B', 'Y' -> Shape.PAPER;
            case 'C', 'Z' -> Shape.SCISSORS;
            default -> null;
        };
    }

    static private Outcome getDesiredOutcome(char c) {
        return switch (c) {
            case 'X' -> Outcome.LOSS;
            case 'Y' -> Outcome.DRAW;
            case 'Z' -> Outcome.WIN;
            default -> null;
        };
    }

    static private Outcome winner(Shape opponent, Shape you) {
        if (opponent.equals(you)) {
            return Outcome.DRAW;
        }
        if ((opponent.equals(Shape.ROCK) && you.equals(Shape.PAPER)) ||
            (opponent.equals(Shape.PAPER) && you.equals(Shape.SCISSORS)) ||
            (opponent.equals(Shape.SCISSORS) && you.equals(Shape.ROCK)) ) {
            return Outcome.WIN;
        }
        return Outcome.LOSS;
    }

    static private Integer calculateScore(Outcome res, Shape yourSelection) {
        int score = 0;

        switch (res) {
            case WIN -> score += 6;
            case DRAW -> score += 3;
            case LOSS -> {}
        }

        switch (yourSelection) {
            case ROCK -> score += 1;
            case PAPER -> score += 2;
            case SCISSORS -> score += 3;
        }

        return score;
    }

    static private Shape calculateSelection(Outcome desiredOutcome, Shape opponentSelection) {
        switch (desiredOutcome) {
            case WIN -> {
                if (opponentSelection.equals(Shape.ROCK)) {
                    return Shape.PAPER;
                } else if (opponentSelection.equals(Shape.PAPER)) {
                    return Shape.SCISSORS;
                } else {
                    return Shape.ROCK;
                }
            }
            case LOSS -> {
                if (opponentSelection.equals(Shape.ROCK)) {
                    return Shape.SCISSORS;
                } else if (opponentSelection.equals(Shape.PAPER)) {
                    return Shape.ROCK;
                } else {
                    return Shape.PAPER;
                }
            }
            case DRAW -> {
                return opponentSelection;
            }
        }
        return null;
    }

    @Override
    public String puzzle1(List<String> input) {
        Integer totalScore = 0;
        for (String round: input) {
            Shape opponentSelection = getShape(round.charAt(0));
            Shape yourSelection = getShape(round.charAt(2));
            totalScore += calculateScore(winner(opponentSelection, yourSelection), yourSelection);
        }
        return Integer.toString(totalScore);
    }

    @Override
    public String puzzle2(List<String> input) {
        Integer totalScore = 0;
        for (String round: input) {
            Shape opponentSelection = getShape(round.charAt(0));
            Shape yourSelection = calculateSelection(getDesiredOutcome(round.charAt(2)), opponentSelection);
            totalScore += calculateScore(winner(opponentSelection, yourSelection), yourSelection);
        }
        return Integer.toString(totalScore);
    }

    @Override
    public String getName(){
        return "RockPaperScissors";
    }
}
