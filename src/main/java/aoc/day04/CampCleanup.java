package aoc.day04;

import aoc.Day;
import aoc.utils.Range;
import java.util.List;

public class CampCleanup extends Day{

    private static Range extractRange(String sRange){
        int start = Integer.parseInt(sRange.split("-")[0]);
        int end = Integer.parseInt(sRange.split("-")[1]);

        return new Range(start, end);
    }

    @Override
    public String puzzle1(List<String> input) {
        int overlaps = 0;
        for (String cleaningPairs : input) {
            Range firstRange = extractRange(cleaningPairs.split(",")[0]);
            Range secondRange = extractRange(cleaningPairs.split(",")[1]);

            if (firstRange.contains(secondRange) || secondRange.contains(firstRange)) {
                overlaps++;
            }
        }
        return Integer.toString(overlaps);
    }

    @Override
    public String puzzle2(List<String> input) {
        int overlaps = 0;
        for (String cleaningPairs : input) {
            Range firstRange = extractRange(cleaningPairs.split(",")[0]);
            Range secondRange = extractRange(cleaningPairs.split(",")[1]);

            if (firstRange.overlaps(secondRange)) {
                overlaps++;
            }
        }
        return Integer.toString(overlaps);
    }

    @Override
    public String getName() {
        return "CampCleanup";
    }
}
