package aoc.day07;

import java.util.LinkedList;
import java.util.List;

public class FileSystem {
    private final Directory root;

    private Directory workingDir;

    public List<Directory> getAllDirectoriesRecursively() {
        List<Directory> dirs = new LinkedList<>();
        for (AbstractFile content : root.getContentsRecursive()) {
            if (content instanceof Directory) {
                dirs.add((Directory) content);
            }
        }
        return dirs;
    }

    public FileSystem() {
        this.root = new RootDirectory();
        this.workingDir = this.root;
    }

    public void mkdir(String dirName) {
        this.workingDir.addContent(new Directory(workingDir, dirName));
    }

    public void addFile(int size, String name) {
        this.workingDir.addContent(new File(workingDir, name, size));
    }

    public void cd(String dirName) {
        switch (dirName) {
            case ".." -> this.workingDir = this.workingDir.parent;
            case "/" -> this.workingDir = this.root;
            default -> {
                AbstractFile f = workingDir.find(dirName);
                if (f instanceof Directory) {
                    workingDir = (Directory) f;
                }
            }
        }
    }

    public String toString() {
        return this.workingDir.toString();
    }

    public long getSize() {
        return this.root.getSize();
    }
}
