package aoc.day07;

public class RootDirectory extends Directory {
    public RootDirectory() {
        super(null, "/");
    }

    public void updateSize(long change) {
        this.size += change;
    }
}
