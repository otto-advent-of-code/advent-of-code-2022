package aoc.day07;

import java.util.*;

public class Directory extends AbstractFile {
    private final List<AbstractFile> contents;

    public void updateSize(long change) {
        this.size += change;
        this.parent.updateSize(change);
    }

    public AbstractFile find(String name) {
        for (AbstractFile content : contents) {
            if (Objects.equals(content.getName(), name)) {
                return content;
            }
        }
        return null;
    }

    public Directory(Directory parent, String name) {
        super(parent, name, 0);
        this.contents = new ArrayList<>();
        this.size = 0;
    }

    public void addContent(AbstractFile content) {
        this.contents.add(content);
        if (content instanceof File) {
            this.updateSize(content.size);
        }
    }

    public List<AbstractFile> getContentsRecursive() {
        List<AbstractFile> allContent = new LinkedList<>(this.contents);

        for (AbstractFile content : this.contents) {
            if (content instanceof File) {
                allContent.add(content);
            } else if (content instanceof Directory) {
                allContent.addAll(((Directory) content).getContentsRecursive());
            }
        }

        return allContent;
    }

    public long getSize() {
        return this.size;
    }

    @Override
    public String toString() {
        StringBuilder repr = new StringBuilder();
        repr.append(String.format("- %s (dir, size=%d)", this.name, this.size));

        for (AbstractFile file : this.contents) {

            if (file instanceof File) {

                repr.append("\n  ").append(file);

            } else if (file instanceof Directory) {

                Scanner scanner = new Scanner(file.toString());
                while (scanner.hasNextLine()) {
                    String innerFile = scanner.nextLine();
                    repr.append("\n").append("  ").append(innerFile);
                }
                scanner.close();
            }
        }
        return repr.toString();
    }
}
