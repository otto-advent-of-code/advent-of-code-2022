package aoc.day07;

public class File extends AbstractFile {

    public File(Directory parent, String name, int size) {
        super(parent, name, size);
    }

    public long getSize() {
        return this.size;
    }

    @Override
    public String toString() {
        return String.format("- %s (file, size=%d)", this.name, this.size);
    }

}
