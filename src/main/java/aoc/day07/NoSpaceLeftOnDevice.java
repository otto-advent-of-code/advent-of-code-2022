package aoc.day07;

import aoc.Day;

import java.util.Arrays;
import java.util.List;

public class NoSpaceLeftOnDevice extends Day {

    private final FileSystem fileSystem;

    public NoSpaceLeftOnDevice() {
        this.fileSystem = new FileSystem();
    }

    private void buildFileSystem(List<String> terminalOutput) {
        for (int i = 1; i < terminalOutput.size(); ++i) {
            String[] command = terminalOutput.get(i).split(" ");
            switch (command[1]) {
                case "ls" -> {
                    int j = i;
                    while (++j < terminalOutput.size()) {
                        String[] file = terminalOutput.get(j).split(" ");
                        if (file[0].equals("$")) {
                            break;
                        } else if (file[0].equals("dir")) {
                            System.out.println(Arrays.toString(file));
                            fileSystem.mkdir(file[1]);
                        } else {
                            fileSystem.addFile(Integer.parseInt(file[0]), file[1]);
                        }
                    }
                    i = j - 1;
                }
                case "cd" -> this.fileSystem.cd(command[2]);
                default -> {
                    System.out.println("Invalid command: " + terminalOutput.get(i));
                    return;
                }
            }
        }
    }

    @Override
    public String puzzle1(List<String> input) {
        buildFileSystem(input);
        this.fileSystem.cd("/");
        System.out.println(this.fileSystem);
        System.out.println();
        List<Directory> allDirs = this.fileSystem.getAllDirectoriesRecursively();

        long sizeSum = 0;
        for (Directory dir : allDirs) {
            long dirSize = dir.getSize();
            if (dirSize <= 100000) {
                sizeSum += dir.getSize();
            }
        }
        return Long.toString(sizeSum);
    }

    @Override
    public String puzzle2(List<String> input) {
        buildFileSystem(input);
        this.fileSystem.cd("/");
        System.out.println(this.fileSystem);
        System.out.println();
        long neededSpace = 30000000 - (70000000 - this.fileSystem.getSize());
        List<Directory> allDirs = this.fileSystem.getAllDirectoriesRecursively();
        long smallestValidDirSize = Long.MAX_VALUE;
        Directory smallestDir = null;

        for (Directory dir : allDirs) {
            long dirSize = dir.getSize();
            if (dirSize > neededSpace && dirSize < smallestValidDirSize) {
                smallestValidDirSize = dirSize;
                smallestDir = dir;
            }
        }

        if (smallestDir != null) {
            System.out.printf("Smallest Directory larger than %d: %s (size=%d)%n",
                    neededSpace,
                    smallestDir.getName(),
                    smallestDir.getSize()
            );
        } else {
            System.out.println("WTF!!!!!!!!!!");
        }
        System.out.println(smallestValidDirSize);
        return Long.toString(smallestValidDirSize);
    }

    @Override
    public String getName() {
        return "NoSpaceLeftOnDevice";
    }
}
