package aoc.day07;

public abstract class AbstractFile {
    protected Directory parent;
    protected String name;
    protected long size;

    public AbstractFile(Directory parent, String name, int size) {
        this.parent = parent;
        this.name = name;
        this.size = size;
    }

    abstract public long getSize();

    abstract public String toString();

    public String getName() {
        return this.name;
    }

}
