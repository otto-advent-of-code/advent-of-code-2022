package aoc.day09;

import aoc.Day;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.abs;

public class RopeBridge extends Day {

    private enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    private final List<Point> knots;
    private final Set<Point> visitedPoints;

    public RopeBridge() {
        this.knots = new ArrayList<>();

        this.visitedPoints = new HashSet<>();
        this.visitedPoints.add(new Point(0, 0));
    }

    private void moveHead(Direction dir) {
        switch (dir) {
            case UP -> this.knots.get(0).y--;
            case DOWN -> this.knots.get(0).y++;
            case LEFT -> this.knots.get(0).x--;
            case RIGHT -> this.knots.get(0).x++;
            default -> throw new IllegalStateException("Unexpected value: " + dir);
        }

    }

    private void moveKnot(int pos) {
        int relativeTailX = this.knots.get(pos).x - this.knots.get(pos - 1).x;
        int relativeTailY = this.knots.get(pos).y - this.knots.get(pos - 1).y;

        // move vertically
        if (relativeTailX == 0) {
            // Move up
            if (relativeTailY > 1) {
                this.knots.get(pos).translate(0, -1);
            }
            // Move down
            else if (relativeTailY < -1) {
                this.knots.get(pos).translate(0, 1);
            }
        }
        // move horizontally
        else if (relativeTailY == 0) {
            // Move left
            if (relativeTailX > 1) {
                this.knots.get(pos).translate(-1, 0);
            }
            // Move right
            if (relativeTailX < -1) {
                this.knots.get(pos).translate(1, 0);
            }
        }
        // move diagonally
        else if (abs(relativeTailX) > 1 || abs(relativeTailY) > 1) {
            this.knots.get(pos).translate(0, (relativeTailY > 0 ? -1 : +1));
            this.knots.get(pos).translate((relativeTailX > 0 ? -1 : +1), 0);
        }
    }

    private void executeMove(Direction dir, int distance) {
        for (int i = 0; i < distance; i++) {
            moveHead(dir);
            for (int j = 1; j < this.knots.size(); j++) {
                moveKnot(j);
            }
            this.visitedPoints.add(
                    new Point(
                            this.knots.get(this.knots.size() - 1).x,
                            this.knots.get(this.knots.size() - 1).y
                    )
            );
        }
    }

    private Direction parseDirection(char dir) {
        switch (dir) {
            case 'U' -> {
                return Direction.UP;
            }
            case 'D' -> {
                return Direction.DOWN;
            }
            case 'L' -> {
                return Direction.LEFT;
            }
            case 'R' -> {
                return Direction.RIGHT;
            }
            default -> throw new IllegalStateException("Unexpected value: " + dir);
        }
    }

    private void addKnots(int amount) {
        for (int i = 0; i < amount; i++) {
            this.knots.add(new Point(0, 0));
        }
    }

    @Override
    public String puzzle1(List<String> input) {

        addKnots(2);

        for (String move : input) {
            int moveDist = Integer.parseInt(move.split(" ")[1]);
            Direction moveDir = parseDirection(move.split(" ")[0].charAt(0));

            executeMove(moveDir, moveDist);
        }
        return Integer.toString(this.visitedPoints.size());
    }

    @Override
    public String puzzle2(List<String> input) {

        addKnots(10);

        for (String move : input) {
            int moveDist = Integer.parseInt(move.split(" ")[1]);
            Direction moveDir = parseDirection(move.split(" ")[0].charAt(0));

            executeMove(moveDir, moveDist);
        }
        return Integer.toString(this.visitedPoints.size());
    }

    @Override
    public String getName() {
        return "RopeBridge";
    }
}
