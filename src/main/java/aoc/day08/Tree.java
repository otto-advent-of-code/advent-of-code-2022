package aoc.day08;

import aoc.utils.ConsoleColors;

public class Tree {

    private final int height;
    private boolean visible = false;

    public Tree(int height) {
        this.height = height;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible() {
        this.visible = true;
    }

    public int getHeight() {
        return this.height;
    }

    public String toString() {

        if (this.visible) {
            return ConsoleColors.GREEN_BRIGHT + this.height + ConsoleColors.RESET;
        }

        return ConsoleColors.RED_BRIGHT + this.height + ConsoleColors.RESET;
    }


}
