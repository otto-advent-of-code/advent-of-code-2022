package aoc.day08;

import java.util.ArrayList;
import java.util.List;

public class Forrest {
    List<List<Tree>> trees = new ArrayList<>();

    private Tree treeAt(int x, int y) {
        return this.trees.get(y).get(x);
    }

    public Forrest(List<String> trees) {
        for (String inputLine : trees) {
            List<Tree> treeLine = new ArrayList<>();
            for (char tree : inputLine.toCharArray()) {
                treeLine.add(new Tree(Character.getNumericValue(tree)));
            }
            this.trees.add(treeLine);
        }
    }

    public int countVisible() {
        int visibleTrees = 0;
        for (List<Tree> treeLine : this.trees) {
            for (Tree tree : treeLine) {
                if (tree.isVisible()) {
                    visibleTrees++;
                }
            }
        }
        return visibleTrees;
    }

    public void checkWesternVisibility() {
        int forrestHeight = this.trees.size();
        int forrestWidth = this.trees.get(0).size();

        for (int y = 0; y < forrestHeight; y++) {
            int tallestTreeHeight = -1;
            for (int x = 0; x < forrestWidth; x++) {
                Tree tree = this.treeAt(x, y);
                if (tree.getHeight() > tallestTreeHeight) {
                    tree.setVisible();
                    tallestTreeHeight = tree.getHeight();
                }

                if (tallestTreeHeight == 9) {
                    break;
                }
            }
        }
    }

    public void checkNorthenVisibility() {
        int forrestHeight = this.trees.size();
        int forrestWidth = this.trees.get(0).size();

        for (int x = 0; x < forrestWidth; x++) {
            int tallestTreeHeight = -1;
            for (int y = 0; y < forrestHeight; y++) {
                Tree tree = this.treeAt(x, y);
                if (tree.getHeight() > tallestTreeHeight) {
                    tree.setVisible();
                    tallestTreeHeight = tree.getHeight();
                }

                if (tallestTreeHeight == 9) {
                    break;
                }
            }
        }
    }

    public void checkEasternVisibility() {
        int forrestHeight = this.trees.size();
        int forrestWidth = this.trees.get(0).size();

        for (int y = 0; y < forrestHeight; y++) {
            int tallestTreeHeight = -1;
            for (int x = forrestWidth - 1; x >= 0; x--) {
                Tree tree = this.treeAt(x, y);
                if (tree.getHeight() > tallestTreeHeight) {
                    tree.setVisible();
                    tallestTreeHeight = tree.getHeight();
                }

                if (tallestTreeHeight == 9) {
                    break;
                }
            }
        }
    }

    public void checkSouthernVisibility() {
        int forrestHeight = this.trees.size();
        int forrestWidth = this.trees.get(0).size();

        for (int x = 0; x < forrestWidth; x++) {
            int tallestTreeHeight = -1;
            for (int y = forrestHeight - 1; y >= 0; y--) {
                Tree tree = this.treeAt(x, y);
                if (tree.getHeight() > tallestTreeHeight) {
                    tree.setVisible();
                    tallestTreeHeight = tree.getHeight();
                }

                if (tallestTreeHeight == 9) {
                    break;
                }
            }
        }
    }

    public String toString() {

        StringBuilder str = new StringBuilder();

        for (List<Tree> treeLine : trees) {
            for (Tree tree : treeLine) {
                str.append(tree);
            }
            str.append("\n");
        }
        return str.toString();
    }

    private int calculateScenicScoreAt(int xPos, int yPos) {
        int forrestHeight = this.trees.size();
        int forrestWidth = this.trees.get(0).size();
        int treeHeight = trees.get(yPos).get(xPos).getHeight();

        int northenScore = 0;
        for (int y = yPos - 1; y >= 0; y--) {
            northenScore++;
            if (this.treeAt(xPos, y).getHeight() >= treeHeight) {
                break;
            }
        }
        if (northenScore == 0) {
            return 0;
        }

        int southernScore = 0;
        for (int y = yPos + 1; y < forrestHeight; y++) {
            southernScore++;
            if (this.treeAt(xPos, y).getHeight() >= treeHeight) {
                break;
            }
        }
        if (southernScore == 0) {
            return 0;
        }

        int westernScore = 0;
        for (int x = xPos - 1; x >= 0; x--) {
            westernScore++;
            if (this.treeAt(x, yPos).getHeight() >= treeHeight) {
                break;
            }
        }
        if (westernScore == 0) {
            return 0;
        }

        int easternScore = 0;
        for (int x = xPos + 1; x < forrestWidth; x++) {
            easternScore++;
            if (this.treeAt(x, yPos).getHeight() >= treeHeight) {
                break;
            }
        }
        if (easternScore == 0) {
            return 0;
        }

        int scenicScore = northenScore * southernScore * easternScore * westernScore;
        return scenicScore;
    }

    public int getHighestScenicScore() {
        int highestScenicScore = 0;
        for (int y = 0; y < this.trees.size(); y++) {
            for (int x = 0; x < this.trees.get(y).size(); x++) {
                int newScenicScore = this.calculateScenicScoreAt(x, y);

                if (newScenicScore > highestScenicScore) {
                    highestScenicScore = newScenicScore;
                }
            }
        }
        return highestScenicScore;
    }

}
