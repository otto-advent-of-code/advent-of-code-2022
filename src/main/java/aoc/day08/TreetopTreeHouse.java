package aoc.day08;

import aoc.Day;

import java.util.List;

public class TreetopTreeHouse extends Day {

    @Override
    public String puzzle1(List<String> input) {
        Forrest forrest = new Forrest(input);

        forrest.checkNorthenVisibility();
        forrest.checkSouthernVisibility();
        forrest.checkEasternVisibility();
        forrest.checkWesternVisibility();

        System.out.println(forrest);
        return Integer.toString(forrest.countVisible());
    }

    @Override
    public String puzzle2(List<String> input) {
        Forrest forrest = new Forrest(input);

        return Integer.toString(forrest.getHighestScenicScore());
    }

    @Override
    public String getName() {
        return "TreetopTreeHouse";
    }
}
