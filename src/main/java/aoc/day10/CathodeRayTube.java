package aoc.day10;

import aoc.Day;
import aoc.utils.ConsoleColors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Math.abs;

public class CathodeRayTube extends Day {

    int signalStrength = 0;
    private int pc;
    private final List<Instruction> program;

    private final Map<Character, Integer> registers;

    private final StringBuilder crt;

    public CathodeRayTube() {
        this.pc = 0;
        this.registers = new HashMap<>();
        this.program = new ArrayList<>();
        this.crt = new StringBuilder("\n");
    }

    public int getRegister(char reg) {
        return this.registers.get(reg);
    }

    public void setRegister(char reg, int val) {
        this.registers.put(reg, val);
    }

    private void runProgram() {
        for (Instruction instruction : this.program) {
            for (int i = 0; i < instruction.cycles(); i++) {
                this.pc++;
                // During cycle
                // System.out.printf("During PC %d: %d\n", this.pc, this.registers.get('X'));
                if (this.pc <= 220 && this.pc % 40 == 20) {
                    this.signalStrength += (this.pc * this.registers.get('X'));
                    // System.out.printf("PC = %d, signal strength = %d\n", this.pc, res);
                }

                //Draw "pixel"
                if (abs(this.registers.get('X') - ((pc - 1) % 40)) <= 1) {
                    this.crt.append(ConsoleColors.GREEN_BACKGROUND)
                            .append("#")
                            .append(ConsoleColors.RESET);
                } else {
                    this.crt.append(ConsoleColors.BLACK_BACKGROUND)
                            .append(".")
                            .append(ConsoleColors.RESET);
                }

                // New line?
                if (this.pc % 40 == 0) {
                    this.crt.append("\n");
                }

                //instruction complete?
                if (i == instruction.cycles() - 1) {
                    instruction.execute(this);
                }

                // After cycle
                // System.out.printf("After PC %d: %d\n", this.pc, this.registers.get('X'));
            }
        }
    }

    private void parseProgram(List<String> programInput) {
        for (String line : programInput) {

            String instructionName = line.split(" ")[0];
            switch (instructionName) {
                case "addx" -> {
                    int operand = Integer.parseInt(line.split(" ")[1]);
                    this.program.add(new Add('X', operand));
                }
                case "noop" -> this.program.add(new Noop());
            }
        }
    }

    @Override
    public String puzzle1(List<String> input) {
        this.registers.put('X', 1);

        this.parseProgram(input);
        this.runProgram();

        return Integer.toString(this.signalStrength);
    }

    @Override
    public String puzzle2(List<String> input) {
        this.registers.put('X', 1);

        this.parseProgram(input);
        this.runProgram();

        return this.crt.toString();
    }

    @Override
    public String getName() {
        return "CathodeRayTube";
    }
}
