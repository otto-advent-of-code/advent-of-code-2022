package aoc.day10;

public class Noop implements Instruction {

    @Override
    public void execute(CathodeRayTube systemState) {
    }

    @Override
    public int cycles() {
        return 1;
    }

    public String toString() {
        return "NOOP";
    }
}
