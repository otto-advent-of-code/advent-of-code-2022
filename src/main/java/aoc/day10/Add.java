package aoc.day10;

public class Add implements Instruction {
    private final int val;

    private final char reg;

    public Add(char reg, int val) {
        this.reg = reg;
        this.val = val;
    }

    @Override
    public void execute(CathodeRayTube systemState) {
        int oldVal = systemState.getRegister(reg);
        systemState.setRegister(reg, oldVal + val);
    }

    @Override
    public int cycles() {
        return 2;
    }

    public String toString() {
        return String.format("ADD %c %d", this.reg, this.val);
    }
}
