package aoc.day10;

public interface Instruction {
    void execute(CathodeRayTube systemState);

    int cycles();

}
