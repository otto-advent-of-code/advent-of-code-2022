package aoc.day04;

import aoc.DayTest;
import aoc.TestCase;
import aoc.TestCaseKey;
import aoc.TestCases;

import java.util.List;

class CampCleanupTest extends DayTest {
    protected static List<TestCase> provideP1TestData() {
        return TestCases.getTestCasesForPuzzle(4, TestCaseKey.Puzzle.ONE);
    }

    protected static List<TestCase> provideP2TestData() {
        return TestCases.getTestCasesForPuzzle(4, TestCaseKey.Puzzle.TWO);
    }

}