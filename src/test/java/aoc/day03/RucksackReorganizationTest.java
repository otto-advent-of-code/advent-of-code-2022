package aoc.day03;

import aoc.DayTest;
import aoc.TestCase;
import aoc.TestCaseKey;
import aoc.TestCases;

import java.util.List;

public class RucksackReorganizationTest extends DayTest {
    protected static List<TestCase> provideP1TestData() {
        return TestCases.getTestCasesForPuzzle(3, TestCaseKey.Puzzle.ONE);
    }

    protected static List<TestCase> provideP2TestData() {
        return TestCases.getTestCasesForPuzzle(3, TestCaseKey.Puzzle.TWO);
    }
}
