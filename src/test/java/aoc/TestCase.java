package aoc;

import java.io.*;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public class TestCase {

    String inputFileName;
    public String expectedResult;
    public int dayNumber;

    public TestCase(String inputFileName, String expectedResult, int dayNumber) {
        this.inputFileName = inputFileName;
        this.expectedResult = expectedResult;
        this.dayNumber = dayNumber;
    }

    public List<String> getInput() {
        try (BufferedReader fileReader = new BufferedReader((new InputStreamReader(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(inputFileName)))))) {
            return fileReader.lines().collect(toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
