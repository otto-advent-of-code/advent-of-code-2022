package aoc.day02;

import aoc.DayTest;
import aoc.TestCase;
import aoc.TestCaseKey;
import aoc.TestCases;

import java.util.List;

class RockPaperScissorsTest extends DayTest {

    protected static List<TestCase> provideP1TestData() {
        return TestCases.getTestCasesForPuzzle(2, TestCaseKey.Puzzle.ONE);
    }

    protected static List<TestCase> provideP2TestData() {
        return TestCases.getTestCasesForPuzzle(2, TestCaseKey.Puzzle.TWO);
    }
}