package aoc;

import java.util.*;

public class TestCases {

    static private final Map<TestCaseKey, List<TestCase>> TESTCASES;

    static {
        TESTCASES = new HashMap<>();
        //Day 1
        TESTCASES.put(new TestCaseKey(1, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day01/case_01.in", "24000", 1)
        ));
        TESTCASES.put(new TestCaseKey(1, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day01/case_01.in", "45000", 1)
        ));

        // Day 2
        TESTCASES.put(new TestCaseKey(2, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day02/case_01.in", "15", 2)
        ));

        TESTCASES.put(new TestCaseKey(2, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day02/case_01.in", "12", 2)
        ));

        // Day 3
        TESTCASES.put(new TestCaseKey(3, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day03/case_01.in", "157", 3)
        ));

        TESTCASES.put(new TestCaseKey(3, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day03/case_01.in", "70", 3)
        ));

        // Day 4
        TESTCASES.put(new TestCaseKey(4, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day04/Case_01.in", "2", 4)
        ));

        TESTCASES.put(new TestCaseKey(4, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day04/Case_01.in", "4", 4)
        ));

        // Day 5
        TESTCASES.put(new TestCaseKey(5, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day05/Case_01.in", "CMZ", 5)
        ));

        TESTCASES.put(new TestCaseKey(5, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day05/Case_01.in", "MCD", 5)
        ));

        // Day 6
        TESTCASES.put(new TestCaseKey(6, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day06/Case_01.in", "7", 6),
                new TestCase("day06/Case_02.in", "5", 6),
                new TestCase("day06/Case_03.in", "6", 6),
                new TestCase("day06/Case_04.in", "10", 6),
                new TestCase("day06/Case_05.in", "11", 6)
        ));

        TESTCASES.put(new TestCaseKey(6, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day06/Case_02.in", "23", 6),
                new TestCase("day06/Case_03.in", "23", 6),
                new TestCase("day06/Case_01.in", "19", 6),
                new TestCase("day06/Case_04.in", "29", 6),
                new TestCase("day06/Case_05.in", "26", 6)
        ));

        // Day 7
        TESTCASES.put(new TestCaseKey(7, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day07/Case_01.in", "95437", 7)
        ));

        TESTCASES.put(new TestCaseKey(7, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day07/Case_01.in", "24933642", 7)
        ));

        // Day 8
        TESTCASES.put(new TestCaseKey(8, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day08/Case_01.in", "21", 8)
        ));

        TESTCASES.put(new TestCaseKey(8, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day08/Case_01.in", "8", 8)
        ));

        // Day 9
        TESTCASES.put(new TestCaseKey(9, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day09/Case_01.in", "13", 9)
        ));

        TESTCASES.put(new TestCaseKey(9, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day09/Case_01.in", "1", 9),
                new TestCase("day09/Case_02.in", "36", 9)
        ));

        // Day 10
        TESTCASES.put(new TestCaseKey(10, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day10/Case_01.in", "0", 10),
                new TestCase("day10/Case_02.in", "13140", 10)
        ));

        TESTCASES.put(new TestCaseKey(10, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day10/Case_02.in",
                        """
                                                                
                                [42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m
                                [42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m
                                [42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m
                                [42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m
                                [42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m
                                [42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[42m#[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m[40m.[0m
                                """,
                        10)
        ));

        // Day 11
        TESTCASES.put(new TestCaseKey(11, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day11/Case_01.in", "10605", 11)
        ));

        TESTCASES.put(new TestCaseKey(11, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day11/Case_01.in", "2713310158", 11)
        ));

        // Day 12
        TESTCASES.put(new TestCaseKey(12, TestCaseKey.Puzzle.ONE), List.of(
                new TestCase("day12/Case_01.in", "31", 12)
        ));

        TESTCASES.put(new TestCaseKey(12, TestCaseKey.Puzzle.TWO), List.of(
                new TestCase("day12/Case_01.in", "29", 12)
        ));

    }

    static public List<TestCase> getTestCasesForPuzzle(Integer day, TestCaseKey.Puzzle puzzle) {
        return TESTCASES.get(new TestCaseKey(day, puzzle));
    }

    static public List<TestCase> getAllForPuzzle(TestCaseKey.Puzzle puzzle) {
        List<TestCase> allTestCasesForPuzzle = new ArrayList<>();

        for (Map.Entry<TestCaseKey, List<TestCase>> entry : TESTCASES.entrySet()) {
            if (entry.getKey().getPuzzle().equals(puzzle)) {
                allTestCasesForPuzzle.addAll(entry.getValue());
            }
        }
        return allTestCasesForPuzzle;
    }
}

