package aoc.day01;

import aoc.DayTest;
import aoc.TestCase;
import aoc.TestCases;
import aoc.TestCaseKey;

import java.util.List;

class CalorieCountingTest extends DayTest {

    protected static List<TestCase> provideP1TestData() {
        return TestCases.getTestCasesForPuzzle(1, TestCaseKey.Puzzle.ONE);
    }

    protected static List<TestCase> provideP2TestData() {
        return TestCases.getTestCasesForPuzzle(1, TestCaseKey.Puzzle.TWO);
    }
}