package aoc.day08;

import aoc.DayTest;
import aoc.TestCase;
import aoc.TestCaseKey;
import aoc.TestCases;

import java.util.List;

class TreetopTreeHouseTest extends DayTest {
    protected static List<TestCase> provideP1TestData() {
        return TestCases.getTestCasesForPuzzle(8, TestCaseKey.Puzzle.ONE);
    }

    protected static List<TestCase> provideP2TestData() {
        return TestCases.getTestCasesForPuzzle(8, TestCaseKey.Puzzle.TWO);
    }

}