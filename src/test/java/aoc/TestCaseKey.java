package aoc;

public class TestCaseKey {
    public enum Puzzle {
        ONE,
        TWO
    }
    private final int day;
    private final TestCaseKey.Puzzle puzzle;

    public TestCaseKey(int day, TestCaseKey.Puzzle puzzle) {
        this.day = day;
        this.puzzle = puzzle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestCaseKey key)) return false;
        return day == key.day && puzzle == key.puzzle;
    }

    @Override
    public int hashCode() {
        int result = day;
        if (puzzle == TestCaseKey.Puzzle.TWO) {
            result = result + (1 << 5);
        }
        return result;
    }

    public Integer getDay() {
        return day;
    }

    public TestCaseKey.Puzzle getPuzzle() {
        return puzzle;
    }
}
