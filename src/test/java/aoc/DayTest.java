package aoc;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DayTest {

    @ParameterizedTest
    @MethodSource("provideP1TestData")
    void testPuzzle1(TestCase testCase) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Day dayToTest = App.DAYS.get(testCase.dayNumber).getDeclaredConstructor().newInstance();
        System.out.printf("Testing puzzle 1 for %s.\n", dayToTest.getName());
        assertEquals(testCase.expectedResult, dayToTest.puzzle1(testCase.getInput()));
        System.out.println("Test OK.\n");
    }

    @ParameterizedTest
    @MethodSource("provideP2TestData")
    void testPuzzle2(TestCase testCase) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Day dayToTest = App.DAYS.get(testCase.dayNumber).getDeclaredConstructor().newInstance();
        System.out.printf("Testing puzzle 2 for %s.\n", dayToTest.getName());
        assertEquals(testCase.expectedResult, dayToTest.puzzle2(testCase.getInput()));
        System.out.println("Test OK.\n");
    }

    protected static List<TestCase> provideP1TestData() {
        return new ArrayList<>(TestCases.getAllForPuzzle(TestCaseKey.Puzzle.ONE));
    }

    protected static List<TestCase> provideP2TestData() {
        return new ArrayList<>(TestCases.getAllForPuzzle(TestCaseKey.Puzzle.TWO));
    }
}